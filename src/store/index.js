import Vue from 'vue';
import Vuex from 'vuex';
// eslint-disable-next-line import/no-cycle,no-unused-vars
import axios from '../axios-auth';
// eslint-disable-next-line import/no-cycle,no-unused-vars
import router from '../router';
import auth from './auth';
import trips from './trips';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    trips,
  },
});
