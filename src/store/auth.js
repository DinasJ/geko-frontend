// eslint-disable-next-line import/no-cycle
import Vue from 'vue';
import axios from '../axios-auth';
import router from '../router';

const state = {
  accessToken: null,
  user: {
    person: {},
  },
};

const getters = {
  // eslint-disable-next-line no-unused-vars,no-shadow
  currentUser(state, getters, rootState, rootGetters) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.accessToken !== null;
  },
  accessToken(state) {
    return state.accessToken;
  },
};

const mutations = {
  SET_AUTH(state, userData) {
    state.accessToken = userData.accessToken;
    state.user = userData.user;
  },
  PURGE_AUTH(state) {
    state.accessToken = null;
    state.user = {};
  },
  UPDATE_USER_PHOTO(state, newPhotoPath) {
    state.user.person = { ...state.user.person, photo: newPhotoPath };
    localStorage.setItem('currentUser', JSON.stringify(state.user));
  },
  REMOVE_USER_PHOTO(state) {
    Vue.set(state.user.person, 'photo', null);
    localStorage.setItem('currentUser', JSON.stringify(state.user));
  },
};

const actions = {
  uploadUserPhoto({ commit }, fd) {
    console.log(fd.get('photo'));
    axios
      .post('api/users/currentPhoto', fd, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(({ data }) => {
        console.log(data);
        commit('UPDATE_USER_PHOTO', data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  removeUserPhoto({ commit }) {
    axios
      .delete('api/users/currentPhoto')
      .then(({ data }) => {
        console.log(data);
        commit('REMOVE_USER_PHOTO');
      })
      .catch((error) => {
        console.log(error);
      });
  },

  async signUp({ commit }, authData) {
    return axios
      .post('/api/register', {
        first_name: authData.first_name,
        last_name: authData.last_name,
        email: authData.email,
        password: authData.password,
      })
      .then((res) => {
        console.log(res);
        localStorage.setItem('accessToken', res.data.success.token);
        localStorage.setItem('currentUser', JSON.stringify(res.data.user));
        commit('SET_AUTH', {
          accessToken: res.data.success.token,
          user: res.data.user,
        });
        router.push('/');
      });
  },
  logIn({ commit }, authData) {
    axios
      .post('/api/login', {
        email: authData.email,
        password: authData.password,
      })
      .then((res) => {
        console.log(res);
        localStorage.setItem('accessToken', res.data.success.token);
        localStorage.setItem('currentUser', JSON.stringify(res.data.user));
        commit('SET_AUTH', {
          accessToken: res.data.success.token,
          user: res.data.user,
        });
        router.push('/');
      })
      .catch((error) => console.log(error));
  },
  checkAuth({ commit }) {
    const accessToken = localStorage.getItem('accessToken');
    const user = JSON.parse(localStorage.getItem('currentUser'));
    if (!accessToken) {
      return;
    }
    commit('SET_AUTH', {
      accessToken,
      user,
    });
  },
  logout({ commit }) {
    commit('PURGE_AUTH');
    commit('trips/PURGE_USER_TRIPS', null, { root: true });
    // todo PURGE_TRIPS to remove search results of last user?
    localStorage.removeItem('accessToken');
    localStorage.removeItem('currentUser');
    // eslint-disable-next-line no-unused-vars
    router.replace('/login').catch((error) => {});
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
