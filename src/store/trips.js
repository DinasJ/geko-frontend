import Vue from 'vue';
import axios from '../axios-auth';
import { formatDate } from '../util/date';

const getDefaultTrip = () => ({
  route_points: [
    { order: 0, city: '', lat: null, lng: null },
    { order: 1, city: '', lat: null, lng: null },
  ],
  date: formatDate(new Date(), 'yyyy-MM-dd'),
  time: formatDate(new Date(), 'HH:mm'),
  seats_available: 3,
  price_per_seat: null,
});

const state = {
  newTripItem: getDefaultTrip(),
  editedTrip: {},
  viewedTrip: {},
  searched_trips: [],
  // todo implement last search
  last_search: {
    from: '',
    to: '',
  },
  user_trips: null,
  errors: {},
};

const getters = {
  searchedTrips(state) {
    return state.searched_trips;
  },
  lastSearch(state) {
    return state.last_search;
  },

  userTrips(state) {
    if (!state.user_trips) return null;
    let sortedTrips = JSON.parse(JSON.stringify(state.user_trips));
    sortedTrips = sortedTrips.sort(
      (a, b) => a.date.localeCompare(b.date) || a.time.localeCompare(b.time)
    );
    const res = sortedTrips.reduce(function (result, current) {
      // eslint-disable-next-line no-param-reassign
      result[current.date] = result[current.date] || [];
      result[current.date].push(current);
      return result;
    }, {});
    return res;
  },
  userTripsRequestPending(state) {
    if (!state.user_trips) return null;
    let sortedTrips = JSON.parse(JSON.stringify(state.user_trips));
    sortedTrips = sortedTrips.sort(
      (a, b) => a.date.localeCompare(b.date) || a.time.localeCompare(b.time)
    );
    sortedTrips = sortedTrips.filter((trip) => {
      // eslint-disable-next-line no-param-reassign
      trip.trip_passengers = trip.trip_passengers.filter(
        (passenger) => passenger.request_status === 'pending'
      );
      return trip.trip_passengers.length;
    });
    const res = sortedTrips.reduce(function (result, current) {
      // eslint-disable-next-line no-param-reassign
      result[current.date] = result[current.date] || [];
      result[current.date].push(current);
      return result;
    }, {});
    return res;
  },
  userTripsRequestAccepted(state) {
    if (!state.user_trips) return null;
    let sortedTrips = JSON.parse(JSON.stringify(state.user_trips));
    sortedTrips = sortedTrips.sort(
      (a, b) => a.date.localeCompare(b.date) || a.time.localeCompare(b.time)
    );
    sortedTrips = sortedTrips.filter((trip) => {
      // eslint-disable-next-line no-param-reassign
      trip.trip_passengers = trip.trip_passengers.filter(
        (passenger) => passenger.request_status === 'accepted'
      );
      return trip.trip_passengers;
    });
    const res = sortedTrips.reduce(function (result, current) {
      // eslint-disable-next-line no-param-reassign
      result[current.date] = result[current.date] || [];
      result[current.date].push(current);
      return result;
    }, {});
    console.log(res);
    return res;
  },
};

const mutations = {
  SET_SEARCHED_TRIPS(state, searchResults) {
    state.searched_trips = searchResults;
  },
  REMOVE_USER_TRIP(state, id) {
    const index = state.user_trips.findIndex((x) => x.id === id);
    state.user_trips.splice(index, 1);
  },
  STORE_USER_TRIP(state, createdTrip) {
    if (!state.user_trips) state.user_trips = [];
    state.user_trips.push(createdTrip);
    state.newTripItem = getDefaultTrip();
  },
  UPDATE_USER_TRIP(state, updatedTrip) {
    // const trip = state.user_trips.find((x) => x.id === updatedTrip.id);
    state.user_trips = [
      ...state.user_trips.map((trip) =>
        trip.id !== updatedTrip.id ? trip : { ...trip, ...updatedTrip }
      ),
    ];
    // state.user_trips.splice(trip, 1, updatedTrip);
  },
  // used in auth.js/logout
  PURGE_USER_TRIPS(state) {
    state.user_trips = null;
  },
  PURGE_SEARCHED_TRIPS(state) {
    state.searched_trips = [];
  },
  SET_USER_TRIPS(state, fetchedUserTrips) {
    state.user_trips = fetchedUserTrips;
  },
  SET_EDITED_TRIP(state, item) {
    state.editedTrip = item;
  },
  SET_VIEWED_TRIP(state, item) {
    state.viewedTrip = item;
  },
  SET_VALIDATION_ERRORS(state, errors) {
    state.errors = errors;
  },
  CLEAR_VALIDATION_ERRORS(state, field) {
    Vue.delete(state.errors, field);
  },
  SET_USER_TRIP_REQUEST(state, payload) {
    const newState = JSON.parse(JSON.stringify(state.user_trips));
    const tripIndex = newState.findIndex((x) => x.id === payload.trip_id);
    const passengers = newState[tripIndex].trip_passengers;
    const passengerIndex = passengers.findIndex((x) => x.id === payload.trip_passenger_id);
    passengers[passengerIndex].request_status = 'accepted';
    const accepted = newState[tripIndex].accepted_passengers;
    accepted.push(passengers[passengerIndex]);
    state.user_trips = newState;
  },
  REMOVE_USER_TRIP_REQUEST(state, payload) {
    const tripIndex = state.user_trips.findIndex((x) => x.id === payload.trip_id);
    const passengers = state.user_trips[tripIndex].trip_passengers;
    const passengerIndex = passengers.findIndex((x) => x.id === payload.trip_passenger_id);
    passengers.splice(passengerIndex, 1);
  },
};

const actions = {
  async createUserTrip({ commit }, trip) {
    return axios
      .post('/api/trips/my', trip)
      .then(({ data }) => {
        commit('STORE_USER_TRIP', data);
      })
      .catch((error) => {
        if (error.response?.status === 422) {
          commit('SET_VALIDATION_ERRORS', error.response.data.errors);
        }
        throw error;
      });
  },
  setSearchedTrips({ commit }, payload) {
    commit('SET_SEARCHED_TRIPS', payload);
  },
  async fetchUserTrips({ commit }) {
    return axios
      .get('/api/trips/my')
      .then(({ data }) => {
        console.log(data);
        commit('SET_USER_TRIPS', data);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async updateUserTrip({ commit }, trip) {
    return axios
      .put(`/api/trips/my/update/${trip.id}`, trip)
      .then(({ data }) => {
        commit('UPDATE_USER_TRIP', data);
      })
      .catch((error) => {
        if (error.response?.status === 422) {
          commit('SET_VALIDATION_ERRORS', error.response.data.errors);
        }
        throw error;
      });
  },
  deleteUserTrip({ commit }, tripId) {
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete this trip? This action cannot be undone.')) {
      axios
        .delete('/api/trips/my/delete', {
          params: {
            id: tripId,
          },
        })
        .then((res) => {
          console.log(res);
          commit('REMOVE_USER_TRIP', tripId);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  },
  purgeSearchedTrips({ commit }) {
    commit('PURGE_SEARCHED_TRIPS');
  },
  async editTrip({ state, commit }, id) {
    let trip;
    trip = state.user_trips.find((x) => x.id === id);
    if (!trip) {
      await axios.get(`api/trips/my/${id}`).then(({ data }) => {
        trip = data;
      });
    }
    commit('SET_EDITED_TRIP', trip);
  },
  async viewTrip({ state, commit }, id) {
    let trip;
    const dates = Object.keys(state.searched_trips);
    for (let i = 0; i < dates.length; i += 1) {
      const tripGroup = state.searched_trips[dates[i]];
      for (let j = 0; j < tripGroup.length; j += 1) {
        if (tripGroup[j].id === id) {
          trip = tripGroup[j];
          break;
        }
      }
      if (trip) break;
    }
    if (!trip) {
      await axios.get(`api/trips/view/${id}`).then(({ data }) => {
        trip = data;
      });
    }
    commit('SET_VIEWED_TRIP', trip);
  },
  bookASeat({ commit }, tripId) {
    axios
      .post(`/api/tripPassengers/book/${tripId}`)
      // .then(({ data }) => {
      //   commit('STORE_BOOK_RESPONSE', data);
      // })
      .catch((error) => {
        if (error.response?.status === 422) {
          commit('SET_VALIDATION_ERRORS', error.response.data.errors);
        }
        throw error;
      });
  },
  cancelRequest({ commit }, payload) {
    axios
      .put(`api/trips/requests/my/cancel`, {
        params: {
          trip_id: payload.trip_id,
          user_id: payload.user_id,
        },
      })
      .then(({ data }) => {
        console.log(data);
        commit('REMOVE_USER_TRIP_REQUEST', payload);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async acceptRequest({ commit }, payload) {
    return axios
      .put(`api/trips/${payload.trip_id}\`/requests/update/${payload.trip_passenger_id}\``)
      .then(({ data }) => {
        console.log(data);
        commit('SET_USER_TRIP_REQUEST', payload);
      })
      .catch((error) => {
        console.log(error);
        throw error;
      });
  },
  async declineRequest({ commit }, payload) {
    return axios
      .delete(`api/trips/requests/delete`, {
        params: {
          id: payload.trip_passenger_id,
          trip_id: payload.trip_id,
        },
      })
      .then(({ data }) => {
        console.log(data);
        commit('REMOVE_USER_TRIP_REQUEST', payload);
      })
      .catch((error) => {
        console.log(error);
        throw error;
      });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
