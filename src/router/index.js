import Vue from 'vue';
import VueRouter from 'vue-router';
import SignUp from '../views/auth/SignUp';
import LogIn from '../views/auth/LogIn';
import CreateTrip from '../views/CreateTrip';
import ManageTrips from '../views/ManageTrips';
import Search from '../views/Search';
import Trip from '../views/trip/Trip';
import Profile from '../views/Profile';
import Notifications from '../views/Notifications';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Search,
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp,
  },
  {
    path: '/login',
    name: 'LogIn',
    component: LogIn,
  },
  {
    path: '/create',
    name: 'CreateTrip',
    component: CreateTrip,
    beforeEach(to, from, next) {
      if (to.name !== 'Login' && !this.$store.getters.isAuthenticated) next({ name: 'Login' });
      else next();
    },
  },
  {
    path: '/manage',
    name: 'ManageTrips',
    component: ManageTrips,
    beforeEach(to, from, next) {
      if (to.name !== 'Login' && !this.$store.getters.isAuthenticated) next({ name: 'Login' });
      else next();
    },
    children: [
      {
        path: 'edit/:id',
        component: () => import(/* webpackChunkName: "EditTrip" */ '../views/EditTrip'),
      },
    ],
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEach(to, from, next) {
      if (to.name !== 'Login' && !this.$store.getters.isAuthenticated) next({ name: 'Login' });
      else next();
    },
  },
  {
    path: '/trip/:id',
    name: 'Trip',
    component: Trip,
    children: [
      {
        path: 'view-details',
        name: 'ViewDetails',
        component: () =>
          import(/* webpackChunkName: "ViewTripDetails" */ '../views/trip/ViewTripDetails'),
      },
      {
        path: 'select-pickup',
        component: () =>
          import(/* webpackChunkName: "SelectPickupPoint" */ '../views/trip/SelectPickupPoint'),
      },
    ],
  },
  {
    path: '/notifications',
    name: 'Notifications',
    component: Notifications,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});
// router.beforeEach((to, from, next) => {
//   if (to.name !== from.name) {
//     next();
//   } else {
//     next(false);
//   }
// });

export default router;
