/* eslint-disable import/prefer-default-export */
import { format } from 'date-fns';

export function formatDate(date, dateFormat = 'MMMM d • eeee') {
  if (!date) {
    return '';
  }
  const d = typeof date === 'string' ? new Date(date.replace(/-/g, '/')) : date;
  return format(d, dateFormat);
}
export function formatTime(time, dateFormat = 'HH:mm') {
  if (!time) {
    return '';
  }
  const d = new Date();
  d.setHours(time.substring(0, 2));
  d.setMinutes(time.substring(3, 5));
  return format(d, dateFormat);
}
