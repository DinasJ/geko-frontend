import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import './assets/tailwind.css';
import './assets/styles/index.css';
import { formatDate, formatTime } from './util/date';

Vue.config.productionTip = false;
Vue.prototype.$formatDate = formatDate;
Vue.prototype.$formatTime = formatTime;
Vue.prototype.$serverUrl = 'http://homestead.test/storage/';

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
