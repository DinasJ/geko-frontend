import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://homestead.test/',
});

instance.interceptors.request.use((config) => {
  const token = localStorage.getItem('accessToken');
  if (token) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

instance.interceptors.response.use(
  (res) => res,
  (err) => {
    switch (err.response.status) {
      case 400:
        throw err; // when error handling is added you can remove this line
      // eventBus.$emit(OPEN_SNACKBAR, err.response.data.message);
      default:
        throw err;
    }
  }
);

export default instance;
