module.exports = {
  theme: {
    placeholderColor: {
      primary: '#101010',
      secondary: '#10ff10',
    },
    extend: {
      boxShadow: {
        outline: '0 0 0 3px var(--color-primary-t-50)',
        'outline-1': '0 0 0 1px var(--color-primary-t-50)',
        'outline-moss-1': '0 0 0 1px var(--color-primary-300)',
        'outline-indigo-1': '0 0 0 1px hsl(217, 48%, 78%)',
        'outline-red-1': '0 0 0 1px hsl(0, 95%, 85%)',
        'outline-neutral-1': '0 0 0 1px hsl(0, 0%, 85%)',
        'outline-none': '0 0 0 0 #000000',
      },
    },
  },
  corePlugins: {},
  plugins: [],
};
